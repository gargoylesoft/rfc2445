//
//  EKEvent.swift
//
//  Copyright © 2016 Gargoyle Software, LLC.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//

import Foundation
import EventKit

private extension EKAlarm {
    /// Converts the alarm object into an RFC2445 compatible format.
    /// - Returns: An `[String]` representing the lines of the alarm description.
    /// - SeeAlso: [RFC2445 Alarm Component](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.6.6)
    var RFC2445: [String] {
        var lines: [String] = []

        lines.append("BEGIN:VALARM")
        lines.append("UID:\(NSUUID().UUIDString)")

        if let date = absoluteDate {
            lines.append("TRIGGER;VALUE=DATE-TIME:\(date.RFC2445()))")
        } else {
            // http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.3.6
            let offset = Int(relativeOffset)

            var str = offset < 0 ? "-" : ""

            let week = 604_800

            if offset % week == 0 {
                str += "\(offset / week)W"
            } else {
                let seconds = offset % 60
                let minutes = (offset / 60) % 60
                let hours = (offset / 3_600) % 24
                let days = offset / 86_400

                if days > 0 {
                    str += "\(days)D"
                }

                if hours > 0 || minutes > 0 || seconds > 0 {
                    str += "T"

                    if hours > 0 {
                        str += "\(hours)H"
                    }

                    if minutes > 0 {
                        str += "\(minutes)M"
                    }

                    if seconds > 0 {
                        if hours > 0 && minutes == 0 {
                            str += "0M"
                        }

                        str += "\(seconds)S"
                    }
                }
            }

            lines.append("TRIGGER:\(str)")
        }
        
        lines.append("DESCRIPTION:Reminder")
        lines.append("ACTION:DISPLAY")
        lines.append("END:VALARM")
        
        return lines
    }
}

private extension EKRecurrenceRule {
    /// Converts the recurrence rule into an RFC2445 compatible format.
    /// - Returns: The generated RFC2445 RRULE string.
    /// - SeeAlso: [RFC2445 RRULE](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.8.5.4)
    var RFC2445: String {
        let freq: String
        switch frequency {
        case .Daily:
            freq = "DAILY"
        case .Monthly:
            freq = "MONTHLY"
        case .Weekly:
            freq = "WEEKLY"
        case .Yearly:
            freq = "YEARLY"
        }

        var text = "RRULE:FREQ=\(freq)"

        if interval > 1 {
            text += ";INTERVAL=\(interval)"
        }

        if let end = recurrenceEnd {
            if let date = end.endDate {
                text += ";UNTIL=\(date.RFC2445())"
            } else {
                text += ";COUNT=\(end.occurrenceCount)"
            }
        }

        return text
    }
}

private extension NSDate {
    /// Converts an `NSDate` to an RFC2445 formatted DATE-TIME or DATE
    ///
    /// - Parameter includeTime: Whether to include a time component
    /// - Returns: The formatted RFC2445 string 
    /// - SeeAlso: [RFC2445 DATE](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.3.4)
    /// - SeeAlso: [RFC2445 DATE-TIME](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.3.5)
    func RFC2445(includeTime includeTime: Bool = true) -> String {
        var time = time_t(timeIntervalSince1970)

        let format = includeTime ? "%Y%m%dT%H%M%SZ" : "%Y%m%d"

        var buffer = [Int8](count: 17, repeatedValue: 0)
        strftime_l(&buffer, buffer.count, format, localtime(&time), nil)

        return String.fromCString(buffer)!
    }
}

/// Escapes the TEXT type blocks to add the \ characters that as needed
///
/// - Parameter text: The text to escape.
/// - SeeAlso: [RFC2445 TEXT](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.3.11)
/// - Returns: The escaped text.
private func escapeText(text: String) -> String {
    return text
        .stringByReplacingOccurrencesOfString("\\", withString: "\\\\")
        .stringByReplacingOccurrencesOfString(";", withString: "\\;")
        .stringByReplacingOccurrencesOfString(",", withString: "\\,")
}

/// Folds lines longer than 75 characters
///
/// - Parameter line: The line to fold
/// - SeeAlso: [RFC2445 Content Lines](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.1)
/// - Returns: The folded text
private func foldLine(line: String) -> String {
    var lines: [String] = []
    var start = line.startIndex
    let endIndex = line.endIndex

    let end = start.advancedBy(75, limit: endIndex)
    lines.append(line.substringWithRange(start..<end))
    start = end

    while start != endIndex {
        // Now we use 74, instead of 75, because we have to account for the extra space we're adding
        let end = start.advancedBy(74, limit: endIndex)

        lines.append(" " + line.substringWithRange(start..<end))

        start = end
    }

    return lines.joinWithSeparator("\r\n")
}

public extension EKEvent {
    /// Converts the event into an appropriate RFC2445 Version 2.0 text description.
    /// - Parameter productIdentifier: The PRODID to write into the calendar item.
    /// - Returns: The RFC2445 string
    /// - SeeAlso: [RFC2445 Event Component](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.6.1)
    /// - SeeAlso: [RFC2445 Product Identifier](http://google-rfc-2445.googlecode.com/svn/trunk/rfc2445.html#4.7.3)
    public func toRFC2445(productIdentifier: String) -> String {
        var lines: [String] = []

        lines.append("BEGIN:VCALENDAR")
        lines.append("VERSION:2.0")
        lines.append("PRODID:\(productIdentifier)")
        lines.append("BEGIN:VEVENT")
        lines.append("UID:\(NSUUID().UUIDString)")

        if allDay {
            lines.append("DTSTART;VALUE=DATE:\(startDate.RFC2445(includeTime: false))")
            lines.append("DTEND;VALUE=DATE:\(endDate.RFC2445(includeTime: false))")
        } else {
            lines.append("DTSTART:\(startDate.RFC2445())")
            lines.append("DTEND:\(endDate.RFC2445())")
        }

        lines.append("SUMMARY:\(escapeText(title))")

        if let notes = notes where !notes.characters.isEmpty {
            lines.append("DESCRIPTION:\(escapeText(notes))")
        }

        if let location = location where !location.characters.isEmpty {
            lines.append("LOCATION:\(escapeText(location))")
        }

        if let URL = URL, path = URL.path {
            lines.append("URL:\(escapeText(path))")
        }

        recurrenceRules?.forEach {
            lines.append($0.RFC2445)
        }
        
        alarms?.forEach {
            lines += $0.RFC2445
        }
        
        lines.append("END:VEVENT")
        lines.append("END:VCALENDAR")
        
        return lines.map {
            foldLine($0)
            }.joinWithSeparator("\r\n")
    }

    /// Converts the RFC2445 text block to an `EKEvent` object.
    ///
    /// - Warning: Not all RFC2445 elements are convertible.  For example, iOS can't set the Organizer, nor does it handle the WKST property to set the start of the week.
    /// - Parameter text: The block of text to parse.
    /// - Parameter store: The `EKEventStore` to use when creating the event.
    /// - Parameter calendar: The `EKCalendar` to use for the event.
    /// - Returns: A tuple containing the created event, as well as a list of dates which should be excluded if the event is recurring.
    /// - Throws: An `RFC2445Exception`
    public func fromRFC2445(text: String, store: EKEventStore, calendar: EKCalendar? = nil) throws -> (event: EKEvent, exclusions: [NSDate]?) {
        let rfc = try RFC2445(string: text)

        return (event: rfc.EKEvent(store, calendar: calendar), exclusions: rfc.exclusions)
    }
}