# RFC2445

[![CI Status](http://img.shields.io/travis/Scott Grosch/RFC2445.svg?style=flat)](https://travis-ci.org/Scott Grosch/RFC2445)
[![Version](https://img.shields.io/cocoapods/v/RFC2445.svg?style=flat)](http://cocoapods.org/pods/RFC2445)
[![License](https://img.shields.io/cocoapods/l/RFC2445.svg?style=flat)](http://cocoapods.org/pods/RFC2445)
[![Platform](https://img.shields.io/cocoapods/p/RFC2445.svg?style=flat)](http://cocoapods.org/pods/RFC2445)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RFC2445 is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "RFC2445"
```

## Author

Gargoyle Software, LLC

## License

RFC2445 is available under the MIT license. See the LICENSE file for more info.
